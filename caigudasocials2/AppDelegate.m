//
//  AppDelegate.m
//  caigudasocials2
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "AppDelegate.h"
#import "CTSocialsTestController.h"
#import "CTFacebook.h"
#import "CTVkontakte.h"
#import "CTGooglePlus.h"
#import "CTFacebook.h"
#import "CTSocialNetwork.h"
#import "CTTwitter.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [CTSocialNetwork applicationDidFinishLaunching];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [CTSocialsTestController new];
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
{
    return [CTSocialNetwork application:application openURL:url sourceApplication:sourceApplication annotation:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [CTSocialNetwork applicationDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [CTSocialNetwork applicationWillTerminate];
}

@end
