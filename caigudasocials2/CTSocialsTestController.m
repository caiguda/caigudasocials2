//
//  ViewController.m
//  caigudasocials2
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//
#import "FacebookSDK.h"

#import "CTSocialsTestController.h"
#import "CTFacebook.h"
#import "CTVkontakte.h"
#import "CTTwitter.h"
#import "CTGooglePlus.h"

#define kCTSharedLink                 @"http://google.com/"
#define kCTSharedText                 @"Я пользуюсь гуглом"


@interface CTSocialsTestController ()

- (void)hideActivity;
- (void)showActivity;

@end

@implementation CTSocialsTestController

- (void)hideActivity
{
    activitiView.hidden = YES;
    activitiIndicator.hidden = YES;
    [activitiIndicator stopAnimating];
}

- (void)showActivity
{
    activitiView.hidden = NO;
    activitiIndicator.hidden = NO;
    [activitiIndicator startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideActivity];
}

#pragma mark - Facebook
- (IBAction)didButtonFloginClicked:(id)sender
{
    [self showActivity];

    [[CTFacebook sharedInstance] loginWithCallback:^(id data, NSError *error, BOOL canceled)
    {
        CTShowSimpleAlert(nil, [NSString stringWithFormat:@"data %@\nerror %@\ncanceled %d",data,error,canceled]);
        [self hideActivity];
    }];
}

- (IBAction)didButtonFprofileClicked:(id)sender
{
    [[CTFacebook sharedInstance] userProfileWithCallback:^(CTUserProfile* user, NSError *error, BOOL canceled)
     {
         CTLog(@"data %@", user);
         CTLog(@"error %@", error);
     }];
}


- (IBAction)didButtonFlogoutClicked:(id)sender
{
    [[CTFacebook sharedInstance] logout];
}

- (IBAction)didButtonFpostClicked:(id)sender
{
   
    [[CTFacebook sharedInstance] postOnMyWallWithData:[self shareParams] callback:^(id data, NSError *error, BOOL canceled) {
        NSLog(@"FB posted");
    }];
    
    
    
    
    /*UIImage *image = [UIImage imageNamed:@"testPost.jpg"];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            UIImagePNGRepresentation(image), @"source",
                            nil
                            ];

    [FBRequestConnection startWithGraphPath:@"me/objects/restaurant.restaurant"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                          }];*/
   // FBShareDialogPhotoParams *params = [[FBShareDialogPhotoParams alloc] init];
    
    /*FBRequest *req = [FBRequest requestForUploadPhoto:image];
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error)
        {
            
        }
        else
        {
            NSString *ID = [result nullProtectedObjectForKey:@"id"];
            NSString *path = [NSString stringWithFormat:@"/%@/sharedposts",ID];
            /*[FBRequestConnection startWithGraphPath:path
                                         parameters:nil
                                         HTTPMethod:@"GET"
                                  completionHandler:^(
                                                      FBRequestConnection *connection,
                                                      id result,
                                                      NSError *error
                                                      ) {
                                      NSMutableDictionary *params1 = [NSMutableDictionary dictionary];
                                      
                                      //[params setObject:kCTSharedLink forKey:kFeedLinkKey];
                                      [params1 setObject:kCTSharedText forKey:kFeedMessageKey];
                                      NSString *imagePath = [result nullProtectedObjectForKey:@"source"];

                                      [params1 setObject:imagePath forKey:kFeedPictureURLKey];//testPost.jpg
                                      
                                      [FBRequestConnection startWithGraphPath:@"/me/feed" parameters:[FBGraphObject graphObjectWrappingDictionary:params1] HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                                       {
                                           NSLog(@"!!!");
                                       }];
                                  }];
        }
    }];*/
    //showing an alert for failure
    
   
}
-(void)postDataWithPhoto:(NSString*)photoID {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"I'm totally posting on my own wall!" forKey:@"message"];
    
    if(photoID) {
        [params setObject:photoID forKey:@"object_attachment"];
    }
    
    [FBRequestConnection startForPostWithGraphPath:@"/me"
                             graphObject:(id<FBGraphObject>)[NSDictionary dictionaryWithDictionary:params]
                       completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             [[[UIAlertView alloc] initWithTitle:@"Result"
                                         message:@"Your update has been posted to Facebook!"
                                        delegate:self
                               cancelButtonTitle:@"Sweet!"
                               otherButtonTitles:nil] show];
         } else {
             [[[UIAlertView alloc] initWithTitle:@"Error"
                                         message:@"Yikes! Facebook had an error.  Please try again!"
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil] show];
         }
     }
     ];
}
- (IBAction)didButtonFinviteClicked:(id)sender
{
    [[CTFacebook sharedInstance] sendInviteToFriendWithData:[self shareParams] callback:^(id data, NSError *error, BOOL canceled)
     {
         NSLog(@"Face book error");
    }];
}

- (IBAction)didButtonFfriendsClicked:(id)sender
{
    [[CTFacebook sharedInstance] friendsListWithCallback:^(id data, NSError *error, BOOL cancelled)
     {
         CTLog(@"data %@", data);
         CTLog(@"error %@", error);
     }];
}
//end FaceBook

#pragma mark - Twitter
- (IBAction)didButtonTloginClicked:(id)sender
{
    [[CTTwitter sharedInstance] loginWithCallback:^(id data, NSError *error, BOOL canceled) {
        
    }];
}

- (IBAction)didButtonTprofileClicked:(id)sender
{
    [[CTTwitter sharedInstance] userProfileWithCallback:^(CTUserProfile* user, NSError *error, BOOL canceled)
     {
         CTLog(@"data %@", user);
         CTLog(@"error %@", error);
     }];
}


- (IBAction)didButtonTlogoutClicked:(id)sender
{
    [[CTTwitter sharedInstance] logout];
}

- (IBAction)didButtonTpostClicked:(id)sender
{
    [[CTTwitter sharedInstance] postOnMyWallWithData:[self shareParams] callback:^(id data, NSError *error, BOOL canceled) {
        NSLog(@"TW posted");
    }];
}
//end Twitter

#pragma mark - Vkontakte
- (IBAction)didButtonVloginClicked:(id)sender
{
    [self showActivity];
    [[CTVkontakte sharedInstance] setViewControllerFromPressent:self];
    [[CTVkontakte sharedInstance] loginWithCallback:^(id data, NSError *error, BOOL canceled)
    {
        CTLog(@"data %@", data);
        CTLog(@"error %@", error);
        CTLog(@"canceled %i", canceled);

        [self hideActivity];
    }];
}

- (IBAction)didButtonVprofileClicked:(id)sender
{
    [[CTVkontakte sharedInstance] setViewControllerFromPressent:self];
    [[CTVkontakte sharedInstance] userProfileWithCallback:^(CTUserProfile* user, NSError *error, BOOL canceled)
     {
         CTLog(@"data %@", user);
         CTLog(@"error %@", error);
     }];
}


- (IBAction)didButtonVlogoutClicked:(id)sender
{
    [[CTVkontakte sharedInstance] logout];
}

- (IBAction)didButtonVpostClicked:(id)sender
{
    [[CTVkontakte sharedInstance] setViewControllerFromPressent:self];
    [[CTVkontakte sharedInstance] postOnMyWallWithData:[self shareParams] callback:^(id data, NSError *error, BOOL canceled) {
        NSLog(@"VK posted");
    }];
}


- (IBAction)didButtonVfriendsClicked:(id)sender
{
    //need implement
    [[CTVkontakte sharedInstance] friendsListWithCallback:^(id data, NSError *error, BOOL canceled) {
        NSLog(@"VK friends - %@",data);
    }];
}
//end Vkontakte


#pragma mark - Google
- (IBAction)didButtonGloginClicked:(id)sender
{
    [self showActivity];
    [[CTGooglePlus sharedInstance] loginWithCallback:^(id data, NSError *error, BOOL canceled) {
        
        [self hideActivity];
        
    }];
}

- (IBAction)didButtonGprofileClicked:(id)sender
{
    [[CTGooglePlus sharedInstance] userProfileWithCallback:^(CTUserProfile* user, NSError *error, BOOL canceled)
     {
         CTLog(@"data %@", user);
         CTLog(@"error %@", error);
     }];
}


- (IBAction)didButtonGlogoutClicked:(id)sender
{
    [[CTGooglePlus sharedInstance] logout];
}

- (IBAction)didButtonGpostClicked:(id)sender
{
    [[CTGooglePlus sharedInstance] postOnMyWallWithData:[self shareParams] callback:^(id data, NSError *error, BOOL canceled) {
        NSLog(@"GP posted");
    }];
}


- (IBAction)didButtonGfriendsClicked:(id)sender
{

}
//end Google

- (NSMutableDictionary *)shareParams
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:kCTSharedText forKey:kFeedMessageKey];
    //[params setObject:kCTSharedLink forKey:kFeedLinkKey];
    
    //[params setObject:@"https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-frc3/t1.0-9/s720x720/10151214_1575688192655614_6006534147376684975_n.jpg" forKey:kFeedPictureURLKey];//testPost.jpg

    //[params setObject:@"http://demotivation.me/images/20130616/tov90zum6d8l.jpg" forKey:kFeedPictureURLKey];//testPost.jpg
   // UIImage *image = [UIImage imageNamed:@"testPost.jpg"];
    //[params setObject:image forKey:kFeedImage];
    return params;
}
@end
