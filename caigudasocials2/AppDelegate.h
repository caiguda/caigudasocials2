//
//  AppDelegate.h
//  caigudasocials2
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTSocialsTestController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

