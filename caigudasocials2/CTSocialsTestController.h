//
//  ViewController.h
//  caigudasocials2
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTSocialsTestController : UIViewController
{
    IBOutlet UIView* activitiView;
    IBOutlet UIActivityIndicatorView* activitiIndicator;
}

@property (strong, nonatomic) NSString *objectID;

@end
