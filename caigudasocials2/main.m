//
//  main.m
//  caigudasocials2
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
