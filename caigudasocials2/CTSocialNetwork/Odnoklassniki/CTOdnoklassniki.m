//
//  CTOdnoklassniki.m
//  WebTaxi
//
//  Created by semenag01 on 12/3/14.
//  Copyright (c) 2014 caiguda. All rights reserved.
//

#import "CTOdnoklassniki.h"
#import "NSDictionary+NullProtected.h"

/*NSString * const appId = @"<app id>";
 NSString * const appKey = @"<app key>";
 NSString * const appSecret = @"<secret key>";
 */


@interface CTOdnoklassniki ()<OKSessionDelegate,OKRequestDelegate>
{
    Odnoklassniki *okApi;
    NSArray *permissions;
}
@property (nonatomic, strong, readwrite) Odnoklassniki *okApi;

@end

@implementation CTOdnoklassniki
@synthesize okApi;

CT_IMPLEMENT_SINGLETON(CTOdnoklassniki);

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTOdnoklassniki" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    if(config)
    {
        NSLog(@"Odnoklassniki config %@", config);
        permissions = [config objectForKey:@"permissions"];
        
        NSString *appId = [[config objectForKey:@"appId"] description];
        NSString *appKey = [[config objectForKey:@"appKey"] description];
        NSString *appSecret = [[config objectForKey:@"appSecret"] description];
        okApi = [[Odnoklassniki alloc] initWithAppId:appId andAppSecret:appSecret andAppKey:appKey andDelegate:self];
    }
    if (!permissions)
        permissions = @[@"VALUABLE_ACCESS"];
    
    redirectUrl = [config objectForKey:@"redirect_url"];
    callbacks = [NSMutableDictionary new];
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackLogin];

    if (okApi.isSessionValid)
    {
        [okApi refreshToken];
    }
    else
    {
        [okApi authorize:permissions];
    }
}

- (void)logout
{
    [okApi logout];
}

- (BOOL)isSessionValid
{
    return okApi.isSessionValid;
}

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackPostWall];
    
    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        NSMutableDictionary *shareDic = [NSMutableDictionary new];
        NSString *link = [aPostData nullProtectedObjectForKey:kFeedLinkKey];
        NSString *text = [aPostData nullProtectedObjectForKey:kFeedMessageKey];
        
        [shareDic setNilProtectedObject:link forKey:@"linkUrl"];
        [shareDic setNilProtectedObject:text forKey:@"comment"];
        
        OKRequest *request = [Odnoklassniki requestWithMethodName:@"share.addLink" andParams:shareDic andHttpMethod:@"GET" andDelegate:self];
        //OKRequest *request = [Odnoklassniki requestWithMethodName:@"friends.get" andParams:nil andHttpMethod:@"GET" andDelegate:self];
        [request load];
    };
    
    [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
     {
         if (!error && !canceled)
         {
             block();
         }
         else
         {
             aCallback(nil, error, canceled);
         }
     }];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *redirect_Url = [redirectUrl lowercaseString];
    
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect_Url])
    {
        return [okApi.session handleOpenURL:url];
    }
    return result;
}

- (NSString *)accessToken
{
    return okApi.session.accessToken;
}

#pragma mark - OKSessionDelegate
- (void)okDidLogin
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(okApi.session.accessToken,nil,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

- (void)okDidNotLogin:(BOOL)canceled
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(nil,nil,canceled);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

- (void)okDidNotLoginWithError:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(nil,error,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

- (void)applicationWillTerminate
{
    [okApi logout];
}

- (void)okDidExtendToken:(NSString *)accessToken
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(nil,nil,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

- (void)okDidNotExtendToken:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(nil,error,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

#pragma mark - OKRequestDelegate
- (void)request:(OKRequest *)request didLoad:(id)result
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackPostWall];
    if (callback)
    {
        callback(result,nil,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackPostWall];
    }
}

- (void)request:(OKRequest *)request didFailWithError:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackPostWall];
    if (callback)
    {
        callback(nil,error,NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackPostWall];
    }
}

- (void)applicationDidBecomeActive
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        callback(nil,nil,YES);
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

@end
