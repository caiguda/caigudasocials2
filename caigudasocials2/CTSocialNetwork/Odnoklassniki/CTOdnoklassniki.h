//
//  CTOdnoklassniki.h
//  WebTaxi
//
//  Created by semenag01 on 12/3/14.
//  Copyright (c) 2014 caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Odnoklassniki.h"
#import "CTSocialNetwork.h"

#define CTOdnoklassnikiInstance [CTOdnoklassniki sharedInstance]

@interface CTOdnoklassniki  :CTSocialNetwork
{
}
CT_DECLARE_SINGLETON(CTOdnoklassniki);

@end


//!Help
//http://apiok.ru/wiki/pages/viewpage.action?pageId=79003783
//http://apiok.ru/wiki/pages/viewpage.action?pageId=75989046