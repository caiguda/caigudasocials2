//
//  CTUserProfile.m
//  APITest
//
//  Created by semenAG01 on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile

+(CTUserProfile*)userProfileTest
{
    CTUserProfile* result = [[CTUserProfile alloc] init];
    result.login = @"semenag01";
    result.email = @"semenag01@meta.ua";
    result.lastName = @"Semeniuk";
    result.firstName = @"Alexander";
    result.gender = @"male";
    
    NSString* db = @"01/18/2013";
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"MM/dd/yyyy";
        result.birthday = [formatter dateFromString:db];
    }
    
    return result;
}

- (NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    // Be careful here. You add this as a category to NSDictionary
    // but you get an id back, which means that result
    // might be an NSArray as well!
    if (error != nil) return nil;
    return result;
}

- (NSString *)birthdayWithFormat:(NSString *)format
{
    NSString *result;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    formatter.dateFormat = format;
    
    result = [formatter stringFromDate:_birthday];
    return result;
}

- (NSString *)nameSocialNetwork
{
    NSString *result = nil;
    switch (_typeSocialNetwork) {
        case CTUserSocialNetworkGooglePlus:
            result = @"Google";
            break;
        case CTUserSocialNetworkVkontakte:
            result = @"VK";
            break;
        case CTUserSocialNetworkFacebook:
            result = @"Facebook";
            break;
        case CTUserSocialNetworkTwitter:
            result = @"Twitter";
            break;
            
        default:
            break;
    }
    return result;
}

- (NSString *)typeStringSocialNetwork
{
    NSString *result = nil;
    switch (_typeSocialNetwork) {
        case CTUserSocialNetworkGooglePlus:
            result = @"gp";
            break;
        case CTUserSocialNetworkVkontakte:
            result = @"vk";
            break;
        case CTUserSocialNetworkFacebook:
            result = @"fb";
            break;
        case CTUserSocialNetworkTwitter:
            result = @"tw";
            break;
            
        default:
            break;
    }
    return result;
}

- (NSString *)getCountryCodeByAddress:(NSString *)locationString
{
    if (!locationString) {
        return nil;
    }
    NSString *location = nil;
    
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false", locationString];

    NSURL *wurl = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];

    NSData *data = [NSData dataWithContentsOfURL: wurl];
    
    if (nil == data) {
        NSLog(@"Error: Fail to get data");
    }
    else{

        NSError *error;
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&error];
        
        NSString *resultStatus = [json valueForKey:@"status"];
        
        if ( (nil == error) && [resultStatus isEqualToString:@"OK"] ) {
            
            NSArray *result = [(NSDictionary*)json objectForKey:@"results"];
            
            NSDictionary *values = (NSDictionary*)[result objectAtIndex:0];
            
            NSArray *component = [(NSDictionary*)values objectForKey:@"address_components"];
            
            for(int j=0;j<[component count];j++)
            {
                NSDictionary *parts = (NSDictionary*)[component objectAtIndex:j];
                if([[parts objectForKey:@"types"] containsObject:@"country"])
                {
                    location = CT_NULL_PROTECT([parts objectForKey:@"short_name"]);
                    break;
                }
            }
        }
        
    }
    return location;
}


@end
