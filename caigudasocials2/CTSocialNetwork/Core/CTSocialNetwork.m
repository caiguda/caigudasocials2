//
//  CTSocialNetwork.m
//  FirstPrintShop
//
//  Created by Semen on 21.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"


@implementation CTSocialNetwork
@synthesize redirectUrl;

static NSMutableArray *instansesForRedirectUrl;


- (id)init
{
    self = [super init];
    if (self)
    {
        NSLog(@"CTSocialNetwork init instanse %@", NSStringFromClass([self class]));
        callbacks = [NSMutableDictionary new];
        if (!instansesForRedirectUrl)
        {
            instansesForRedirectUrl = [NSMutableArray new];
        }
        
        [self setup];
        
        if (redirectUrl)
        {
            [instansesForRedirectUrl addObject:self];
        }
    }
    return self;
}


/*
 * need to override this methods
 */
- (void)setup
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (NSString*)accessToken
{
    NSAssert(nil, @"Override this method in subclasses!");
    return @"";
}

// login\logout
- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)logout
{
    NSAssert(nil, @"Override this method in subclasses!");
}
- (BOOL)isSessionValid
{
    NSAssert(nil, @"Override this method in subclasses!");
    return NO;
}

// user profile
- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

+ (void)applicationDidFinishLaunching
{
//    [[CTFacebook sharedInstance] applicationDidFinishLaunching];
    for (CTSocialNetwork *instans in instansesForRedirectUrl)
    {
        [instans applicationDidFinishLaunching];
    }
}

+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    for (CTSocialNetwork *instans in instansesForRedirectUrl)
    {
        NSString *redU = [instans.redirectUrl lowercaseString];
        NSString *urlU = [url.absoluteString lowercaseString];

        if ([urlU hasPrefix:redU])
        {
            return [instans application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    return NO;
}

+ (void)applicationDidBecomeActive
{
    for (CTSocialNetwork *instans in instansesForRedirectUrl)
    {
        [instans applicationDidBecomeActive];
    }
}

+ (void)applicationWillTerminate
{
    for (CTSocialNetwork *instans in instansesForRedirectUrl)
    {
        [instans applicationWillTerminate];
    }
//    [[CTFacebook sharedInstance] applicationWillTerminate];
}

- (void)applicationDidFinishLaunching
{
    ;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSAssert(nil, @"Override this method in subclasses!");
    return YES;
}

- (void)applicationDidBecomeActive
{
    
}

- (void)applicationWillTerminate
{
    
}

- (void)dealloc
{
    [instansesForRedirectUrl removeObject:self];
    if (instansesForRedirectUrl.count == 0)
    {
        instansesForRedirectUrl = nil;
    }
}
@end
