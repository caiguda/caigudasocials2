//
//  CTUserProfile.h
//  APITest
//
//  Created by semenAG01 on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CTUserSocialNetwork
{
    CTUserSocialNetworkGooglePlus = 0,
    CTUserSocialNetworkVkontakte,
    CTUserSocialNetworkFacebook,
    CTUserSocialNetworkTwitter
    
} CTUserSocialNetwork;

@interface CTUserProfile : NSObject

@property (nonatomic, strong) NSString* ID;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* login;
@property (nonatomic, strong) NSString* gender;
@property (nonatomic, strong) NSDate* birthday;
@property (nonatomic, strong) NSURL* avatarURL;
@property (nonatomic, strong) NSString* language;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, assign) CTUserSocialNetwork typeSocialNetwork;
@property (nonatomic, strong) NSString* nameSocialNetwork;

@property (nonatomic, strong) NSDictionary *sourceDictionary;
@property (nonatomic, strong) id userData;

+(CTUserProfile*)userProfileTest;

- (NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation;
- (NSString *)getCountryCodeByAddress:(NSString *)locationString;
- (NSString *)birthdayWithFormat:(NSString *)format;
- (NSString *)typeStringSocialNetwork;

@end
