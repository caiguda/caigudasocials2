//
//  CTSocialNetwork.h
//  FirstPrintShop
//
//  Created by Semen on 21.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTUserProfile.h"
#import "CTSingleton.h"
#import "CTKitDefines.h"

#define kSocialNetworkCallBackLogin          @"kSocialNetworkCallBackLogin"
#define kSocialNetworkCallBackUserData       @"kSocialNetworkCallBackUserData"
#define kSocialNetworkCallBackPostWall       @"kSocialNetworkCallBackPostWall"
#define kSocialNetworkCallBackFriends        @"kSocialNetworkCallBackFriends"


// key for create post dictionary
#define kFeedMessageKey         @"message"
#define kFeedLinkKey            @"link"
#define kFeedPictureURLKey      @"picture"
#define kFeedImage              @"image"
//end




#define kFeedNameKey            @"name"         //can only be used if kFeedLinkKey is specified
#define kFeedDescriptionKey     @"description"  //can only be used if kFeedLinkKey is specified


typedef void (^CTSocialNetworkCallback)(id data, NSError* error, BOOL canceled);

@interface CTSocialNetwork : NSObject
{
    NSMutableDictionary* callbacks;
    NSString *redirectUrl;
}

@property (nonatomic, weak) UIViewController* viewControllerFromPressent;
@property (nonatomic, readonly)  NSString *redirectUrl;

- (NSString*)accessToken;

// login\logout
- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback;
- (void)logout;
- (BOOL)isSessionValid;


// user profile
- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback;

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback;

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback;

// default use this method in app delegate
+ (void)applicationDidFinishLaunching;
+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
+ (void)applicationDidBecomeActive;
+ (void)applicationWillTerminate;

// can use this method for each instans in app delegate
- (void)applicationDidFinishLaunching;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

// this methods use in AppDelegate application:openURL:sourceApplication:annotation:
- (void)applicationDidBecomeActive;
- (void)applicationWillTerminate;

@end
