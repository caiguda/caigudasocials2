//
//  CTUserProfile+Vkontakte.h
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile.h"

@interface CTUserProfile (Vkontakte)

+ (CTUserProfile*)userWithVkontakteDictinary:(NSDictionary*)aDictionary;
- (void)setupWithVkontakteDictinary:(NSDictionary*)aDictionary;

@end
