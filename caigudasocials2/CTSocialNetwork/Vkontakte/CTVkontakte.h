//
//  CTVkontakte.h
//  Pro-Otdyh-New
//
//  Created by Yuriy Bosov on 11/13/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//
#import "CTSocialNetwork.h"
#import <VKSdk.h>

#define CTVkontakteInstance [CTVkontakte sharedInstance]

@interface CTVkontakte : CTSocialNetwork </*VkontakteDelegate,*/VKSdkDelegate>
{
    UIViewController* vkLigonController;
    NSString *token;
}

CT_DECLARE_SINGLETON(CTVkontakte);

@property (nonatomic, weak) UIViewController* viewControllerFromPressent;

@end
