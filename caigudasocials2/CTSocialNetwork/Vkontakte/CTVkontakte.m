//
//  CTVkontakte.m
//  Pro-Otdyh-New
//
//  Created by Yuriy Bosov on 11/13/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//


#import "CTVkontakte.h"
#import "CTHelper.h"
#import "CTKitDefines.h"
#import "VKAuthorizeController.h"
#import "CTUserProfile+Vkontakte.h"

@implementation CTVkontakte

CT_IMPLEMENT_SINGLETON(CTVkontakte);

/*
 *Override method
 */
- (void)setup
{
   
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTVkontakteConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSAssert(config, @"Need config file!");
    
    NSLog(@"vk config %@", config);
    
    NSString* appID = [config objectForKey:@"appID"];
    NSAssert(appID, @"Need appID!");
    
    
    redirectUrl = [config objectForKey:@"redirect_url"];
    NSAssert(redirectUrl, @"Need redirect url!");
    
    [VKSdk initializeWithDelegate:self andAppId:appID];

}

- (NSString*)accessToken
{
    return [VKSdk getAccessToken].accessToken;
}

#pragma mark - Login\Logout
- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    if (![callbacks objectForKey:kSocialNetworkCallBackLogin])
    {
        if ([VKSdk isLoggedIn])
        {
            aCallback([VKSdk getAccessToken].accessToken, nil, NO);
        }
        else
        {
            NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTVkontakteConfiguration" ofType:@"plist"];
            NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
            NSString* permissions = [config objectForKey:@"permissions"];
            NSAssert(permissions, @"Need permissions!");
            NSArray *arrayPermission =[permissions componentsSeparatedByString:@","];

            [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackLogin];
            [VKSdk authorize:arrayPermission revokeAccess:YES];
        }
    }
}

- (void)logout
{
    [callbacks removeAllObjects];
    [VKSdk forceLogout];
}

- (BOOL)isSessionValid
{
    return [VKSdk isLoggedIn];
}

#pragma mark - User profile
- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackUserData];
    
    void(^block)(void) = ^(void)
    {
        NSDictionary *params = @{VK_API_ACCESS_TOKEN : [VKSdk getAccessToken].accessToken,
                                 VK_API_FIELDS :@"country,sex,bdate,lang,photo"
                                 };
        VKRequest *request = [[VKApi users] get:params];
        
        request.completeBlock = ^(VKResponse *response)
        {
            if ([response.json isKindOfClass:[NSArray class]]) {
                NSArray *array = response.json;
                NSDictionary *dictionary = array.firstObject;
                CTUserProfile *user = [CTUserProfile userWithVkontakteDictinary:dictionary];
                aCallback(user,nil,NO);
            }
            else
            {
                aCallback(nil,nil,NO);
            }
        };
        request.errorBlock = ^(NSError *error)
        {
            aCallback(nil,error,NO);
        };
        [request start];
    };
    
    if ([VKSdk isLoggedIn])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackUserData];
                 if (callback)
                     callback(nil, error, canceled);
                 [callbacks removeObjectForKey:kSocialNetworkCallBackUserData];
             }
         }];
    }
}

#pragma mark - Post On My Wall
- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackPostWall];

    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        
        NSString *message = [aPostData nullProtectedObjectForKey:kFeedMessageKey];
        NSString *link = [aPostData nullProtectedObjectForKey:kFeedLinkKey];
        NSString *imageStrUrl = [aPostData nullProtectedObjectForKey:kFeedPictureURLKey];
        UIImage *image = [aPostData nullProtectedObjectForKey:kFeedImage];

        NSMutableDictionary *params = [NSMutableDictionary new];

        [params setObject:[VKSdk getAccessToken].accessToken forKey:VK_API_ACCESS_TOKEN];
        [params setObject:[VKSdk getAccessToken].userId forKey:VK_API_OWNER_ID];

        
        
        if (message)
        {
            [params setObject:message forKey:VK_API_MESSAGE];
        }else
        {
            message = @"";
        }
        
        if (link)
        {
            [params setObject:link forKey:VK_API_ATTACHMENTS];
        }else
        {
            link = @"";
        }
        
        
        VKRequest *request = [[VKApi wall] post:[NSDictionary dictionaryWithDictionary:params]];
        request.completeBlock = ^(VKResponse *response)
        {
            aCallback(response,nil,NO);
        };
        request.errorBlock = ^(NSError *error)
        {
            aCallback(nil,error,NO);
        };
        
        
        if (imageStrUrl || image)
        {
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageStrUrl]];
            if (imageStrUrl)
            {
                image = [UIImage imageWithData:data];
            }
            
            
            VKRequest * requestPhoto = [VKApi uploadWallPhotoRequest:image
                                                     parameters:[VKImageParameters pngImage]
                                                         userId:0
                                                        groupId:0];
            requestPhoto.completeBlock = ^(VKResponse *response)
            {
                NSString *photoId;
                if ([response.json isKindOfClass:[NSArray class]]) {
                    NSArray *array = response.json;
                    photoId = [array.lastObject nullProtectedObjectForKey:@"id"];
                    
                    NSString* attachValue = [NSString stringWithFormat:@"photo%@_%@,%@",[VKSdk getAccessToken].userId,photoId,link];
                    
                    [params setObject:attachValue forKey:VK_API_ATTACHMENTS];
                    
                    VKRequest* r = [[VKApi wall] post:params];
                    r.completeBlock = ^(VKResponse *response)
                    {
                        aCallback(response,nil,NO);
                    };
                    r.errorBlock = ^(NSError *error)
                    {
                        aCallback(nil,error,NO);
                    };
                    [r start];
                }else
                {
                    [request start];
                }
                
            };
            requestPhoto.errorBlock = ^(NSError *error)
            {
                aCallback(nil,error,NO);
            };
            [requestPhoto start];
        }
        else
        {
            [request start];
        }
    };
    
        
    if ([VKSdk isLoggedIn])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

#pragma mark - Login\Logout
- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackFriends];
    
    void(^block)(void) = ^(void)
    {
        NSDictionary *params = @{//VK_API_ACCESS_TOKEN : [VKSdk getAccessToken].accessToken,
                                 @"order" : @"name",
                                 VK_API_USER_ID : [VKSdk getAccessToken].userId,
                                 VK_API_FIELDS :@"country,sex,bdate,lang,photo"
                                 };
        VKRequest *request = [[VKApi friends] get:params];
        
        request.completeBlock = ^(VKResponse *response)
        {
            if ([response.json isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dic = response.json;
               
                NSArray *arr = [dic nullProtectedObjectForKey:@"items"];
                NSMutableArray *users = [NSMutableArray new];
                
                for (NSDictionary *u in arr) {
                    CTUserProfile *user = [CTUserProfile userWithVkontakteDictinary:u];
                    [users addObject:user];
                }
                
                aCallback(users,nil,NO);
            }
            else
            {
                aCallback(nil,nil,NO);
            }
        };
        request.errorBlock = ^(NSError *error)
        {
            aCallback(nil,error,NO);
        };
        [request start];
    };
    
    if ([VKSdk isLoggedIn])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackUserData];
                 if (callback)
                     callback(nil, error, canceled);
                 [callbacks removeObjectForKey:kSocialNetworkCallBackUserData];
             }
         }];
    }
}

#pragma mark - VKSdkDelegate
//required
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    token = expiredToken.accessToken;
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    NSError *error = [NSError errorWithVkError:authorizationError];
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
        callback(nil, error, YES);
    [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self.viewControllerFromPressent presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    token = newToken.accessToken;
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
        callback(token, nil, NO);
    [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
}

//optional
- (void)vkSdkAcceptedUserToken:(VKAccessToken *)aToken
{
        token = aToken.accessToken;
}

- (void)vkSdkRenewedToken:(VKAccessToken *)newToken
{
        token = newToken.accessToken;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *edirect_Url = [redirectUrl lowercaseString];

    if ([[[url absoluteString] lowercaseString] hasPrefix:edirect_Url])
    {
        result = [VKSdk processOpenURL:url fromApplication:sourceApplication];
        
    }
    return result;
}


- (void)applicationDidBecomeActive
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
        callback(nil, nil, YES);
    [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
}

@end
