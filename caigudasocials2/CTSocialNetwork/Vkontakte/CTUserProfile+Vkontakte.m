//
//  CTUserProfile+Vkontakte.m
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile+Vkontakte.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (Vkontakte)

/*
 * vkontakte
 */
+ (CTUserProfile*)userWithVkontakteDictinary:(NSDictionary*)aDictionary
{
    CTUserProfile* user = [[CTUserProfile alloc] init];
    [user setupWithVkontakteDictinary:aDictionary];
    return user;
}

- (void) setupWithVkontakteDictinary:(NSDictionary*)aDictionary
{
    self.typeSocialNetwork = CTUserSocialNetworkVkontakte;
    self.sourceDictionary = aDictionary;
    
    self.ID = [NSString stringWithFormat:@"%@",CT_NULL_PROTECT([aDictionary objectForKey:@"id"])];
    self.firstName = CT_NULL_PROTECT([aDictionary objectForKey:@"first_name"]);
    self.lastName = CT_NULL_PROTECT([aDictionary objectForKey:@"last_name"]);
    if (!self.name.length) {
        if (!self.firstName) {
            self.firstName = @"";
        }
        if (!self.lastName) {
            self.lastName = @"";
        }
        self.name = [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
    }
    NSInteger language = [CT_NULL_PROTECT([aDictionary objectForKey:@"language"]) integerValue];
    NSDictionary *dictCounISO = [self dictionaryWithContentsOfJSONString:@"countries ISO code.txt"];
    NSArray *arr = [dictCounISO objectForKey:@"country"];
    
    NSString *countryName = [((NSDictionary *)CT_NULL_PROTECT([aDictionary objectForKey:@"country"])) nullProtectedObjectForKey:@"title"];
    
    for (NSDictionary *d in arr) {
        NSString *str = [d objectForKey:@"english"];
        if ([str isEqualToString:countryName]) {
            self.location = [d objectForKey:@"alpha2"];
            break;
        }
    }
    
    self.language = @"ru";
    if (language == 0) {
        self.language = @"ru";
    }else if (language == 1) {
        self.language = @"uk";
    }else if (language == 2) {
        self.language = @"be";
    }else if (language == 3) {
        self.language = @"en";
    }
    
    
    NSString *gender = CT_NULL_PROTECT([aDictionary objectForKey:@"sex"]);
    if (gender) {
        self.gender = ([gender intValue] == 0)? @"gemo":@"";
        self.gender = ([gender intValue] == 1)? @"female":@"";
        self.gender = ([gender intValue] == 2)? @"male":@"";
    }
    
    
    NSString* avatarStr = CT_NULL_PROTECT([aDictionary objectForKey:@"photo"]);
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = CT_NULL_PROTECT([aDictionary objectForKey:@"bdate"]);
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"dd.MM.yyyy";
        self.birthday = [formatter dateFromString:db];
    }
}
//end vkontakte

@end
