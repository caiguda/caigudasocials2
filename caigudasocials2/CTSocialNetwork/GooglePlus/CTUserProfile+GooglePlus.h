//
//  CTUserProfile+GooglePlus.h
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile.h"

@class GPPSignIn;

@interface CTUserProfile (GooglePlus)

+ (CTUserProfile*)userWithGooglePlusGPPSignIn:(GPPSignIn*)signIn;
- (void)setupWithGooglePlusGPPSignIn:(GPPSignIn*)signIn;

@end
