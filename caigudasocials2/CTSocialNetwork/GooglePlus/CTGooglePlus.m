//
//  MUGooglePlus.m
//  FirstPrintShop
//
//  Created by Semen on 20.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGooglePlus.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "NSDictionary+NullProtected.h"
#import "CTUserProfile+GooglePlus.h"

@implementation CTGooglePlus

CT_IMPLEMENT_SINGLETON(CTGooglePlus);



/*
 *Override method
 */
- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTGooglePlusConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSAssert(config, @"Need config file!");
    [[GPPShare sharedInstance] setDelegate:self];

    redirectUrl = [config objectForKey:@"redirect_url"];
    NSAssert(redirectUrl, @"Need redirect url!");
    
    NSString* clientId = [config objectForKey:@"clientId"];

    googlePlusSignIn = [GPPSignIn sharedInstance];
    
    googlePlusSignIn.shouldFetchGooglePlusUser = YES;
    googlePlusSignIn.shouldFetchGoogleUserEmail = YES;
    googlePlusSignIn.shouldFetchGoogleUserID = YES;

    
    googlePlusSignIn.clientID = clientId;
    googlePlusSignIn.scopes = [NSArray arrayWithObjects: kGTLAuthScopePlusLogin, nil];
    
    googlePlusSignIn.delegate = self;
}
- (NSString*)accessToken
{
    return googlePlusSignIn.idToken;
}


#pragma mark - Login\Logout
- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    if (![callbacks objectForKey:kSocialNetworkCallBackLogin])
    {
        if ([googlePlusSignIn.authentication accessToken])
        {
            aCallback([googlePlusSignIn idToken], nil, NO);
        }
        else
        {
            [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackLogin];
            [googlePlusSignIn authenticate];
        }
    }
}

- (void)logout
{
    [googlePlusSignIn signOut];
    [callbacks removeAllObjects];
}

- (BOOL)isSessionValid
{
    return [googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser;
}

#pragma mark - User profile
- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackUserData];
    
    void(^block)(void) = ^(void)
    {
        CTUserProfile *user = [CTUserProfile userWithGooglePlusGPPSignIn:googlePlusSignIn];
        
        CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackUserData];
        if (callback)
            callback(user, nil, NO);
        [callbacks removeObjectForKey:kSocialNetworkCallBackUserData];
    };
    
    if ([googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackUserData];
                 if (callback)
                     callback(nil, error, error == nil);
                 [callbacks removeObjectForKey:kSocialNetworkCallBackUserData];
             }
         }];
    }
}


#pragma mark - google plus delegate
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
    {
        if ([auth accessToken])
        {
            callback([auth accessToken], nil, NO);
        }
        else if (error)
        {
            callback(nil, error, NO);
        }
        else
        {
            callback(nil, nil, YES);
        }
        [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
    }
}

- (void)didDisconnectWithError:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
        callback(nil, error, NO);
    [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
}

#pragma mark - Post On My Wall
- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    [callbacks setObject:[aCallback copy] forKey:kSocialNetworkCallBackPostWall];
    
    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        [GPPShare sharedInstance].delegate = self;
        NSString *message = [aPostData nullProtectedObjectForKey:kFeedMessageKey];
        NSString *imageStr = [aPostData nullProtectedObjectForKey:kFeedPictureURLKey];
        NSString *link = [aPostData nullProtectedObjectForKey:kFeedLinkKey];
        UIImage *image = [aPostData nullProtectedObjectForKey:kFeedImage];

        if (!message)
        {
            message = @"";
        }
        id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
        if (link && !imageStr)
        {
            
            [shareBuilder setPrefillText:message];
            [shareBuilder setURLToShare:[NSURL URLWithString:link]];
        }
        if (imageStr || image)
        {
            NSString *shareText = (link)?[NSString stringWithFormat:@"%@\n%@",link,message]:message;
            [shareBuilder setPrefillText:shareText];
            NSData *dataImage = nil;
            if (imageStr) {
                dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageStr]];
            }
            else
            {
                dataImage = UIImagePNGRepresentation(image);
            }
            [shareBuilder attachImageData:dataImage];
        }
        
        [shareBuilder open];

        
    };

    if ([googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

- (void)applicationDidBecomeActive
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackLogin];
    if (callback)
        callback(nil, nil, YES);
    [callbacks removeObjectForKey:kSocialNetworkCallBackLogin];
}

#pragma mark - Protocol Delegate
- (void)finishedSharingWithError:(NSError *)error
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackPostWall];
    if (callback)
        callback(nil, error, NO);
    [callbacks removeObjectForKey:kSocialNetworkCallBackPostWall];
}

- (void)finishedSharing:(BOOL)shared
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:kSocialNetworkCallBackPostWall];
    if (callback)
        callback(nil, nil, NO);
    [callbacks removeObjectForKey:kSocialNetworkCallBackPostWall];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *edirect_Url = [redirectUrl lowercaseString];
    
    if ([[[url absoluteString] lowercaseString] hasPrefix:edirect_Url])
    {
        result = [GPPURLHandler handleURL:url
                        sourceApplication:sourceApplication
                               annotation:annotation];
        
    }
    return result;
}
@end

