//
//  CTGooglePlus.h
//  FirstPrintShop
//
//  Created by Semen on 20.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GooglePlus/GooglePlus.h>
#import "CTSocialNetwork.h"


typedef void (^CTGooglePlusCallback)(id data, NSError* error, BOOL canceled);

@interface CTGooglePlus : CTSocialNetwork <GPPSignInDelegate, GPPShareDelegate>
{
   
    GPPSignIn *googlePlusSignIn;
    
}

CT_DECLARE_SINGLETON(CTGooglePlus);


@end
