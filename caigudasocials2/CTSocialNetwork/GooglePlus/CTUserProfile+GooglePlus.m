//
//  CTUserProfile+GooglePlus.m
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile+GooglePlus.h"

#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (GooglePlus)

/*
 * google
 */
+ (CTUserProfile*)userWithGooglePlusGPPSignIn:(GPPSignIn*)signIn
{
    CTUserProfile* user = [[CTUserProfile alloc] init];
    [user setupWithGooglePlusGPPSignIn:signIn];
    
    return user;
}

- (void)setupWithGooglePlusGPPSignIn:(GPPSignIn*)signIn
{
    self.typeSocialNetwork = CTUserSocialNetworkGooglePlus;
    
    self.ID = CT_NULL_PROTECT(signIn.googlePlusUser.identifier);//
    self.email = CT_NULL_PROTECT(signIn.userEmail);//
    if (!self.email)
    {
        self.email = signIn.authentication.userEmail;
    }
    self.firstName = CT_NULL_PROTECT(signIn.googlePlusUser.name.givenName);//
    self.lastName = CT_NULL_PROTECT(signIn.googlePlusUser.name.familyName);//
    self.name = CT_NULL_PROTECT(signIn.googlePlusUser.displayName);
    if (!self.name && self.name.length) {
        if (!self.firstName) {
            self.firstName = @"";
        }
        if (!self.lastName) {
            self.lastName = @"";
        }
        self.name = [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
    }
    self.login = CT_NULL_PROTECT(signIn.googlePlusUser.nickname);//
    self.gender = CT_NULL_PROTECT(signIn.googlePlusUser.gender);//
    self.language = CT_NULL_PROTECT(signIn.googlePlusUser.language);//
    self.location = CT_NULL_PROTECT(signIn.googlePlusUser.currentLocation);//
    
    NSString* avatarStr = CT_NULL_PROTECT(signIn.googlePlusUser.image.url);
    
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = [signIn.googlePlusUser birthday];//
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"yyyy-MM-dd";
        self.birthday = [formatter dateFromString:db];
    }
}
//end google

@end
