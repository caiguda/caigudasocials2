//
//  CTFacebook.m
//  APITest
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFacebook.h"
#import "FacebookSDK.h"
#import "NSDictionary+NullProtected.h"
#import "CTResponse.h"
#import "CTUserProfile+Facebook.h"

// Graph Path
#define kGraphPathMe                @"me"
#define kGraphPathMeFeed            @"me/feed"
#define kGraphPathMyFriends         @"me/friends"
#define kGraphPathApprequests       @"apprequests"

@interface CTFacebook()

- (void)setup;

@end


@implementation CTFacebook

CT_IMPLEMENT_SINGLETON(CTFacebook);



/*
 *Override method
 */
- (void)setup
{
    NSArray* list = [self permissionsList];
    NSParameterAssert(list);
}

- (NSString*)accessToken
{
    return [FBSession.activeSession accessTokenData].accessToken;
}

#pragma mark - Configurations
- (NSArray*)permissionsList
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTFacebookConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    if(config)
    {
        NSLog(@"facebook config %@", config);
        permissions = [config objectForKey:@"permissions"];
    }
    if (!permissions)
        permissions = [NSArray arrayWithObjects:@"public_profile",@"email", nil];
    
    redirectUrl = [config objectForKey:@"redirect_url"];
    NSAssert(redirectUrl, @"Need redirect url!");

    // override this method in subclasses or used CTFacebookConfiguretion.plist
    return permissions;
}

- (void)fbResync
{
    ACAccountStore *accountStore;
    ACAccountType *accountTypeFB;
    if ((accountStore = [[ACAccountStore alloc] init]) && (accountTypeFB = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook] ) ){
        
        NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFB];
        id account;
        if (fbAccounts && [fbAccounts count] > 0 && (account = [fbAccounts objectAtIndex:0])){
            
            [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                //we don't actually need to inspect renewResult or error.
                if (error)
                {
                    
                }
            }];
        }
    }
}

#pragma mark - Login, Logout, UserData
- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    [FBSession openActiveSessionWithReadPermissions:[self permissionsList]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
    {
        if(error)
        {
            switch (status)
            {
                case FBSessionStateOpen:
                    aCallback(FBSession.activeSession.accessTokenData.accessToken, nil, NO);
                    break;
                    
                case FBSessionStateClosed:
                    aCallback(nil, error, YES);
                    break;
                case FBSessionStateClosedLoginFailed:
                    aCallback(nil, error, YES);
                    [FBSession.activeSession closeAndClearTokenInformation];
                    break;
                    
                default:
                    break;
            }
        }
        else
        {
            switch (status)
            {
                case FBSessionStateOpen:
                    aCallback(FBSession.activeSession.accessTokenData.accessToken, nil, NO);
                    break;
                                
                case FBSessionStateClosed:
                    aCallback(nil, nil, YES);
                    break;
                case FBSessionStateClosedLoginFailed:
                    aCallback(nil, nil, YES);
                    [FBSession.activeSession closeAndClearTokenInformation];
                    break;
                    
                default:
                    break;
            }
        }
    }];
}

- (void)logout
{
    [FBSession.activeSession close];
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
}

- (BOOL)isSessionValid
{
    return [FBSession.activeSession isOpen];
}

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    void(^block)(void) = ^(void)
    {
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
         {
             if (result)
             {
                 CTUserProfile* user = [CTUserProfile userWithFaceBookDictinary:result];
                 
                 
                 [FBRequestConnection startWithGraphPath:@"/me/picture"
                                              parameters:@{@"type":@"large", @"redirect":@"false"}
                                              HTTPMethod:@"GET"
                                       completionHandler:^(
                                                           FBRequestConnection *connection,
                                                           id result,
                                                           NSError *error
                                                           )
                 {
                     if (error)
                     {
                        aCallback(nil, error, NO);
                     }
                     else
                     {
                         NSString* path = [result nullProtectedObjectForKeyPath:@"data.url"];
                         user.avatarURL = [NSURL URLWithString:path];
                         aCallback(user, nil, NO);
                     }
                 }];
             }
             else
             {
                 [self errorHandler:error];
                 aCallback(nil, error, NO);
             }
         }];
    };

    if ([FBSession.activeSession isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
        {
            if (data)
            {
                block();
            }
            else
            {
                aCallback(data, error, canceled);
            }
        }];
    }
}

#pragma mark - Friends List
- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    void(^block)(void) = ^(void)
    {
        FBRequest *r = [FBRequest requestForMyFriends];
        
        [r startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
        {
            CTResponse* response = [CTResponse new];

            if (error)
            {
                response.success = NO;
                response.code = error.code;
                aCallback(response, error, NO);
            }
            else
            {
                if ([result isKindOfClass:[NSDictionary class]]) {
                    NSArray *array = [result nullProtectedObjectForKey:@"data"];
                    NSMutableArray *users = [NSMutableArray new];
                    for (NSDictionary *dic in array)
                    {
                        CTUserProfile *user = [CTUserProfile userWithFaceBookDictinary:dic];
                        [users addObject:user];
                    }
                    response.success = YES;
                    response.boArray = users;
                  
                }
                else
                {
                    response.success = NO;
                }
                aCallback(response, nil, NO);
          }
      }];
    };
    
    if ([FBSession.activeSession isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }
}

#pragma mark - Post On My Wall
- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithDictionary:aPostData];
    
    NSString *message = [postData nullProtectedObjectForKey:kFeedMessageKey];
    if (message) {
        [postData removeObjectForKey:kFeedMessageKey];
        [postData setValue:message forKey:kFeedCaptionKey];
    }
    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:postData]
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {

            if (result == FBWebDialogResultDialogCompleted) {
               
                if ([resultURL.absoluteString isEqualToString:@"fbconnect://success"])
                {
                    aCallback(nil, nil, YES);
                }else
                {
                    aCallback(resultURL, nil, NO);
                }
            }else
            {
                if (error) {
                    aCallback(nil, error, NO);
                }else
                {
                    aCallback(nil, nil, YES);
                }
            }

        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback showDialog:(BOOL)aShowDialog
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithDictionary:aPostData];
    
    NSString *message = [postData nullProtectedObjectForKey:kFeedMessageKey];
    if (message) {
        [postData removeObjectForKey:kFeedMessageKey];
        [postData setValue:message forKey:kFeedCaptionKey];
    }
    void(^block)(void) = ^(void)
    {
        if (aShowDialog)
        {
            [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:postData]
                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
             {
                 [self errorHandler:error];
                 BOOL cancel = (result == FBWebDialogResultDialogNotCompleted  && !resultURL && !error)||
                                (result == FBWebDialogResultDialogCompleted && [resultURL.absoluteString isEqualToString:@"fbconnect://success"]);
                 aCallback(resultURL, error, cancel);
             }];
        }
        else
        {
            
            void(^requestBlock)(void) = ^(void)
            {
                [FBRequestConnection startWithGraphPath:kGraphPathMeFeed parameters:[FBGraphObject graphObjectWrappingDictionary:postData] HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                 {
                     [self errorHandler:error];
                     aCallback(result, error, NO);
                 }];
            };
            
            if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
            {
                [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                      defaultAudience:FBSessionDefaultAudienceFriends
                                                    completionHandler:^(FBSession *session, NSError *error)
                {
                     if (!error)
                     {
                         requestBlock();
                     }
                    else
                    {
                        aCallback(nil, error, NO);
                    }
                 }];
            }
            else
            {
                requestBlock();
            }
        }
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }
}

#pragma mark - Send App Invite
- (void)sendInviteToFriendWithData:(NSDictionary *)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    
    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentDialogModallyWithSession:[FBSession activeSession]
                                               dialog:kGraphPathApprequests
                                           parameters:aPostData
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {
            if (error)
            {
                [self errorHandler:error];
            }
            aCallback(resultURL, error, (result == FBWebDialogResultDialogNotCompleted || [[resultURL absoluteString] hasPrefix: @"fbconnect://success?error_code=4201"]));
        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }

}

- (void)errorHandler:(NSError *)aError
{
    if (!aError)
    {
        return;
    }
    NSString *message = aError.fberrorUserMessage;
    if (!message)
    {
        message = @"No internet connection";
    }

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    CTLog(@"error %@", aError);
}

#pragma mark - AppDelegate Methods
- (void)applicationDidFinishLaunching
{
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
    {
        [FBSession openActiveSessionWithReadPermissions:[self permissionsList]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
        {
            if (error)
            {
                [FBSession.activeSession closeAndClearTokenInformation];
            }
        }];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *edirect_Url = [redirectUrl lowercaseString];
    
    if ([[[url absoluteString] lowercaseString] hasPrefix:edirect_Url])
    {
        return [[FBSession activeSession] handleOpenURL:url];
    }
    return result;
}

- (void)applicationDidBecomeActive
{
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate
{
    [[FBSession activeSession] close];
}

@end