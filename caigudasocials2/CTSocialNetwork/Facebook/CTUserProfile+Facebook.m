//
//  CTUserProfile+Facebook.m
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile+Facebook.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (Facebook)

/*
 * facebook
 */
+ (CTUserProfile*)userWithFaceBookDictinary:(NSDictionary*)aDictionary
{
    CTUserProfile* user = [[CTUserProfile alloc] init];
    [user setupWithFaceBookDictinary:aDictionary];
    return user;
}

- (void)setupWithFaceBookDictinary:(NSDictionary*)aDictionary
{
    self.typeSocialNetwork = CTUserSocialNetworkFacebook;
    self.sourceDictionary = aDictionary;
    self.ID = CT_NULL_PROTECT([aDictionary objectForKey:@"id"]);
    self.email = CT_NULL_PROTECT([aDictionary objectForKey:@"email"]);
    self.firstName = CT_NULL_PROTECT([aDictionary objectForKey:@"first_name"]);
    self.lastName = CT_NULL_PROTECT([aDictionary objectForKey:@"last_name"]);
    self.name = CT_NULL_PROTECT([aDictionary objectForKey:@"name"]);
    if (!self.name && self.name.length) {
        if (!self.firstName) {
            self.firstName = @"";
        }
        if (!self.lastName) {
            self.lastName = @"";
        }
        self.name = [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
    }
    self.login = CT_NULL_PROTECT([aDictionary objectForKey:@"username"]);
    self.gender = CT_NULL_PROTECT([aDictionary objectForKey:@"gender"]);
    
    NSString *locale = CT_NULL_PROTECT([aDictionary objectForKey:@"locale"]);
    
    if (locale) {
        NSRange range = [locale rangeOfString:@"_"];
        if (range.location != NSNotFound) {
            NSRange rangeByLang = NSMakeRange(0, range.location);
            NSRange rangeByLoc = NSMakeRange(range.location+1, locale.length - range.location-1);
            
            self.language = [locale substringWithRange:rangeByLang];
            
            self.location = [locale substringWithRange:rangeByLoc];
        }
    }
    
    NSString* avatarStr = CT_NULL_PROTECT([aDictionary valueForKeyPath:@"picture.data.url"]);
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = CT_NULL_PROTECT([aDictionary objectForKey:@"birthday"]);
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"MM/dd/yyyy";
        self.birthday = [formatter dateFromString:db];
    }
}
//end facebook

@end
