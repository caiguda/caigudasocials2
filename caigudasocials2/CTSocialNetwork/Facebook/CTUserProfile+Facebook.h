//
//  CTUserProfile+Facebook.h
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile.h"

@interface CTUserProfile (Facebook)

+ (CTUserProfile*)userWithFaceBookDictinary:(NSDictionary*)aDictionary;
- (void)setupWithFaceBookDictinary:(NSDictionary*)aDictionary;

@end
