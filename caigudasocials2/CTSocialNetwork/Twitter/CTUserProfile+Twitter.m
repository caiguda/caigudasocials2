//
//  CTUserProfile+Twitter.m
//  caigudasocials2
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "CTUserProfile+Twitter.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (Twitter)

/*
 * twitter
 */
+ (CTUserProfile*)userWithTwitterDictinary:(NSDictionary*)aDictionary
{
    CTUserProfile* user = [[CTUserProfile alloc] init];
    [user setupWithTwitterDictionary:aDictionary];
    return user;
}
- (void)setupWithTwitterDictionary:(NSDictionary*)aDictionary
{
    self.typeSocialNetwork = CTUserSocialNetworkTwitter;
    self.sourceDictionary = aDictionary;
    
    self.ID = [aDictionary nullProtectedObjectForKey:@"id"];
    NSString *name = [aDictionary nullProtectedObjectForKey:@"name"];
    
    self.name = name;
    
    if (name)
    {
        NSRange rangeSp = [name rangeOfString:@" "];
        NSRange range_ = [name rangeOfString:@"_"];
        NSRange range;
        
        if (range_.location == NSNotFound &&
            rangeSp.location == NSNotFound)
        {
            self.firstName = name;
        }
        else
        {
            if (rangeSp.location != NSNotFound)
            {
                range = rangeSp;
            }
            else if (range_.location != NSNotFound)
            {
                range = range_;
            }
            if (range.location != NSNotFound)
            {
                NSRange rangeByFirst = NSMakeRange(0, range.location);
                NSRange rangeByLast = NSMakeRange(range.location+1, name.length - range.location-1);
                
                self.firstName = [name substringWithRange:rangeByFirst];
                
                self.lastName = [name substringWithRange:rangeByLast];
            }
            else
            {
                self.firstName = name;
            }
        }
    }
    self.login = [aDictionary nullProtectedObjectForKey:@"screen_name"];
    self.email = [aDictionary nullProtectedObjectForKey:@"email"];
    self.language = [aDictionary nullProtectedObjectForKey:@"lang"];
    
    NSString *loc = [aDictionary nullProtectedObjectForKey:@"location"];
    self.location = [self getCountryCodeByAddress:loc];
    
    NSString* urlString = [aDictionary nullProtectedObjectForKeyPath:@"profile_image_url"];
    if ([urlString length])
        self.avatarURL = [NSURL URLWithString:urlString];
    
}
//end twitter
@end
