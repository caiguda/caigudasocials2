//
//  ViewController.m
//  SocialServicesProject
//
//  Created by AndrewShmig on 04/20/13.
//  Copyright (c) 2013 AndrewShmig. All rights reserved.
//

#import "TwitterViewController.h"

@implementation TwitterViewController

- (id)initWithCallBack:(CTSocialNetworkCallback)aCallback
{
    self = [super initWithNibName:@"TwitterViewController" bundle:nil];
    callback = [aCallback copy];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // устанавливает WebView в нужной позиции и с нужными размерами
    //CGRect frame = [[UIScreen mainScreen] bounds];
    _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_webView];
    
    // создаем TwitterCommunicator для получения токенов
    _tw = [[ASATwitterCommunicator alloc]
           initWithWebView:_webView];
    
    // инициируем запрос по получению доступа к пользовательскому аккаунту
    __weak TwitterViewController *weakself = self;
    [_tw startOnCancelBlock:^
     {
         TwitterViewController *strongself = weakself;
         
         [strongself dismissViewControllerAnimated:YES completion:^{
             strongself->callback(nil, nil, YES);
         }];
     }
               onErrorBlock:^(NSError *error)
     {
         TwitterViewController *strongself = weakself;
         
         [strongself dismissViewControllerAnimated:YES completion:^{
             strongself->callback(nil, error, NO);
         }];
     }
             onSuccessBlock:^(ASATwitterUserAccount *account)
     {
         TwitterViewController *strongself = weakself;
         
         [strongself dismissViewControllerAnimated:YES completion:^{
             strongself->callback(account, nil, NO);
         }];
     }];
}

@end