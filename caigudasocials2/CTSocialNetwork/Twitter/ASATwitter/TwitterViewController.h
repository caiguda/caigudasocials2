//
//  ViewController.h
//  SocialServicesProject
//
//  Created by AndrewShmig on 04/20/13.
//  Copyright (c) 2013 AndrewShmig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASATwitterCommunicator.h"
#import "CTTwitter.h"

@interface TwitterViewController : UIViewController
{
    CTSocialNetworkCallback callback;
}

@property UIWebView *webView;
@property ASATwitterCommunicator *tw;

- (id)initWithCallBack:(CTSocialNetworkCallback)aCallback;

@end