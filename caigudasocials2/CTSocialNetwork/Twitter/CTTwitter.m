//
//  CTTwitter.m
//  OutSpeak
//
//  Created by Alexander Burkhai on 9/27/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTwitter.h"
#import "TwitterViewController.h"
#import "CTUserProfile+Twitter.h"
#import "ASATwitterUserAccount.h"

#define kCTTwitterOAuthTokenKey                 @"kCTTwitterOAuthTokenKey"
#define kCTTwitterOAuthTokenSecretKey           @"kCTTwitterOAuthTokenSecretKey"
#define kCTTwitterUserIDKey                     @"kCTTwitterUserIDKey"
#define kCTTwitterScreenName                    @"kCTTwitterScreenName"

@interface CTTwitter ()
{
    NSString *consumerKey;
    NSString *consumerSecret;
    
    ASATwitterUserAccount *account;
}

@end

@implementation CTTwitter

CT_IMPLEMENT_SINGLETON(CTTwitter);

@synthesize oauthToken, oauthTokenSecret;
@synthesize consumerKey,consumerSecret;


- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"CTTwitterConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    CTLog(@"twitter config %@", config);
    consumerKey = [config valueForKey:@"consumer_key"];
    consumerSecret = [config valueForKey:@"consumer_secret"];
    
    NSParameterAssert(consumerKey);
    NSParameterAssert(consumerSecret);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    account = [[ASATwitterUserAccount alloc] initWithToken:[defaults objectForKey:kCTTwitterOAuthTokenKey]
                                               tokenSecret:[defaults objectForKey:kCTTwitterOAuthTokenSecretKey]
                                             twitterUserID:[defaults objectForKey:kCTTwitterUserIDKey]
                                            userScreenName:[defaults objectForKey:kCTTwitterScreenName]];
    if (![self isSessionValid])
        account = nil;
}

- (BOOL)isSessionValid
{
    return
    account.oauthToken.length &&
    account.oauthTokenSecret.length &&
    account.twitterUserID.length &&
    account.screenName.length;
}

- (NSString *)oauthToken
{
    return account.oauthToken;
}

- (NSString *)oauthTokenSecret
{
    return account.oauthTokenSecret;
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    __weak __typeof(&*self) weakSelf = self;
    CTSocialNetworkCallback block = ^(id data, NSError* error, BOOL canceled)
    {
        if (data && !error && !canceled)
        {
            if ([data isKindOfClass:[ASATwitterUserAccount class]])
            {
                [weakSelf setupAccount:data];
            }
        }
        
        aCallback(data,error,canceled);
    };
    
    if ([self isSessionValid])
    {
        aCallback(account, nil, NO);
    }
    else
    {
        TwitterViewController *vc = [[TwitterViewController alloc] initWithCallBack:block];
        if (CT_IS_IPAD)
        {
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
            //vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        }
        if (self.viewControllerFromPressent)
        {
            [self.viewControllerFromPressent presentViewController:vc animated:YES completion:nil];
        }
        else
        {
            [CTGetPrimeViewController() presentViewController:vc animated:YES completion:nil];
        }
        
    }
}

#pragma mark - Logout

- (void)clearAllTwitterCookies {
    
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [cookieStorage cookies])
    {
        if ([[cookie domain] rangeOfString:@"twitter.com"].location != NSNotFound) {
            [cookieStorage deleteCookie:cookie];
        }
    }
}

- (void)clearAccessToken
{
    account = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kCTTwitterOAuthTokenKey];
    [defaults removeObjectForKey:kCTTwitterOAuthTokenSecretKey];
    [defaults removeObjectForKey:kCTTwitterUserIDKey];
    [defaults removeObjectForKey:kCTTwitterScreenName];
    [defaults synchronize];
}

- (void)logout
{
    twitterAPI = nil;
    [self clearAllTwitterCookies];
    [self clearAccessToken];
}

- (void)setupAccount:(ASATwitterUserAccount*)anAccount
{
    account = anAccount;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:account.oauthToken forKey:kCTTwitterOAuthTokenKey];
    [defaults setObject:account.oauthTokenSecret forKey:kCTTwitterOAuthTokenSecretKey];
    [defaults setObject:account.twitterUserID forKey:kCTTwitterUserIDKey];
    [defaults setObject:account.screenName forKey:kCTTwitterScreenName];
    
    [defaults synchronize];
}

#pragma mark - UserProfile

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    assert(aCallback);
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey consumerSecret:consumerSecret oauthToken:anAccount.oauthToken oauthTokenSecret:anAccount.oauthTokenSecret];
        
        [twitterAPI getUsersShowForUserID:anAccount.twitterUserID orScreenName:anAccount.screenName includeEntities:@(NO) successBlock:^(NSDictionary *userDictionary)
         {
             CTUserProfile *user = [CTUserProfile userWithTwitterDictinary:userDictionary];
             aCallback(user, nil, NO);
         }
                               errorBlock:^(NSError *error)
         {
             aCallback(nil, error, NO);
         }];
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
    
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback;
{
    assert(aCallback);
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey consumerSecret:consumerSecret oauthToken:anAccount.oauthToken oauthTokenSecret:anAccount.oauthTokenSecret];
        
        [twitterAPI getFollowersListForUserID:anAccount.twitterUserID
                                 orScreenName:anAccount.screenName
                                        count:@"10000"
                                       cursor:nil
                                   skipStatus:nil
                          includeUserEntities:nil
                                 successBlock:^(NSArray *users, NSString *previousCursor, NSString *nextCursor) {
                                     if (users.count) {
                                         NSMutableArray *models = [NSMutableArray new];
                                         for (NSDictionary *dic in users)
                                         {
                                             CTUserProfile *p = [CTUserProfile userWithTwitterDictinary:dic];
                                             [models addObject:p];
                                         }
                                         aCallback(models, nil, NO);
                                     }else
                                     {
                                         aCallback(nil, nil, NO);
                                     }
                                     
                                 } errorBlock:^(NSError *error) {
                                     aCallback(nil, error, NO);
                                 }];    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
    
}

- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    assert(aCallback);
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        NSString *message = [aPostData nullProtectedObjectForKey:kFeedMessageKey];
        NSString *imageStr = [aPostData nullProtectedObjectForKey:kFeedPictureURLKey];
        NSString *link = [aPostData nullProtectedObjectForKey:kFeedLinkKey];
        UIImage *image = [aPostData nullProtectedObjectForKey:kFeedImage];
        
        
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey
                                                       consumerSecret:consumerSecret
                                                           oauthToken:anAccount.oauthToken
                                                     oauthTokenSecret:anAccount.oauthTokenSecret];
        NSMutableString *status = [NSMutableString stringWithString:@""];
        if (message) {
            status = [NSMutableString stringWithString:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        if (link)
        {
            if ([status length])
                [status appendString:@"\n"];
            
            [status appendString:[link stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        NSURL *urlImage = [NSURL URLWithString:imageStr];
        if (urlImage || image)
        {
            if (urlImage) {
                [twitterAPI postStatusUpdate:status
                           inReplyToStatusID:nil
                                    mediaURL:urlImage
                                     placeID:nil
                                    latitude:nil
                                   longitude:nil
                         uploadProgressBlock:nil
                                successBlock:^(NSDictionary *status) {
                                    if (aCallback)
                                        aCallback(status, nil, NO);
                                } errorBlock:^(NSError *error) {
                                    if (aCallback)
                                        aCallback(nil, error, NO);
                                }];
            }else
            {
                [twitterAPI postStatusUpdate:status mediaDataArray:[NSArray arrayWithObject:UIImagePNGRepresentation(image)] possiblySensitive:nil inReplyToStatusID:nil latitude:nil longitude:nil placeID:nil displayCoordinates:nil uploadProgressBlock:nil successBlock:^(NSDictionary *status) {
                    if (aCallback)
                        aCallback(status, nil, NO);
                } errorBlock:^(NSError *error) {
                    if (aCallback)
                        aCallback(nil, error, NO);
                }];
            }
            
        }else
        {
            [twitterAPI postStatusUpdate:status
                       inReplyToStatusID:nil
                                latitude:nil
                               longitude:nil
                                 placeID:nil
                      displayCoordinates:@(NO)
                                trimUser:@(NO)
                            successBlock:^(NSDictionary *status)
             {
                 if (aCallback)
                     aCallback(status, nil, NO);
             } errorBlock:^(NSError *error)
             {
                 if (aCallback)
                     aCallback(nil, error, NO);
             }];
        }
        
        
        
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
}

//postDirectMessage

- (void)sendInviteToFriend:(NSString*)aUserLogin withData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        NSString *message = [aPostData nullProtectedObjectForKey:kFeedMessageKey];
        NSString *link = [aPostData nullProtectedObjectForKey:kFeedLinkKey];
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey
                                                       consumerSecret:consumerSecret
                                                           oauthToken:anAccount.oauthToken
                                                     oauthTokenSecret:anAccount.oauthTokenSecret];
        NSMutableString *status = [NSMutableString stringWithString:@""];
        if (message) {
            status = [NSMutableString stringWithString:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        if (link)
        {
            if ([status length])
                [status appendString:@"\n"];
            
            [status appendString:[link stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [twitterAPI postDirectMessage:status forScreenName:aUserLogin orUserID:nil successBlock:^(NSDictionary *message)
        {
            aCallback(message,nil,NO);
            
        } errorBlock:^(NSError *error)
        {
            aCallback(nil,error,NO);
        }];
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
}
@end
