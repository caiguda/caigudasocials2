Pod::Spec.new do |s|
    
	#root
		s.name     = 'CaigudaSocials'
		s.summary  = 'Social API for FB, TW, G+,VK'
		s.version  = '0.0.7'
		s.license   = { :type => 'Apache', :file => 'LICENSE.txt' }
	
		s.homepage = 'http://caiguda.com'
		s.author   = 'Caiguda Software Studio'
		s.source   = { :git => 'https://bitbucket.org/caiguda/caigudasocials2.git', :tag => '0.0.7' }
	
	#platform
		s.platform = :ios, '6.0' 
		s.ios.deployment_target = '6.0'
	
	#build settings 
	  	s.requires_arc = true
  
  	#file patterns
		# s.source_files = 'caigudasocials2/CTSocialNetwork/**/*.{h,m}'
	
	#subspecs
		s.subspec 'Core' do |core|
			core.dependency 'CaigudaKit'
			core.source_files = 'caigudasocials2/CTSocialNetwork/Core/**/*.{h,m}'
	        core.resources = "caigudasocials2/CTSocialNetwork/**/*.{jpeg,png,xib,nib,txt}"
	    end
		
		s.subspec 'Facebook' do |facebook|
			facebook.dependency 'CaigudaSocials/Core'
			facebook.dependency 'Facebook-iOS-SDK','3.18.0'
			facebook.source_files = 'caigudasocials2/CTSocialNetwork/Facebook/**/*.{h,m}'
		end
		
		s.subspec 'Twitter' do |twitter|
			twitter.dependency 'CaigudaSocials/Core'
			twitter.dependency 'STTwitter'
			twitter.source_files = 'caigudasocials2/CTSocialNetwork/Twitter/**/*.{h,m}'
		end
		
		s.subspec 'Vkontakte' do |vkontakte|
			vkontakte.dependency 'CaigudaSocials/Core'
			vkontakte.dependency 'VK-ios-sdk'
			vkontakte.source_files = 'caigudasocials2/CTSocialNetwork/Vkontakte/**/*.{h,m}'
		end

		s.subspec 'GooglePlus' do |googleplus|
			googleplus.dependency 'CaigudaSocials/Core'
			googleplus.source_files = 'caigudasocials2/CTSocialNetwork/GooglePlus/**/*.{h,m}'
			googleplus.vendored_frameworks = 'Frameworks/GooglePlus.framework', 'Frameworks/GoogleOpenSource.framework'
			googleplus.resource = "Frameworks/GooglePlus.bundle"
			googleplus.framework = 'AssetsLibrary', 'CoreLocation', 'CoreMotion', 'CoreGraphics', 'CoreText', 'MediaPlayer', 'Security', 'SystemConfiguration', 'AddressBook'
			googleplus.xcconfig = { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/caigudasocials2/Frameworks/GoogleOpenSource.framework/Versions/A/Headers"' }
		end

		s.subspec 'Odnoklassniki' do |odnoklassniki|
			odnoklassniki.dependency 'CaigudaSocials/Core'
			odnoklassniki.dependency 'odnoklassniki_ios_sdk'
			odnoklassniki.source_files = 'caigudasocials2/CTSocialNetwork/Odnoklassniki/**/*.{h,m}'
		end

end
	